# file dataAnalysis.py
# __author__ = 'oliver'

import sqlite3
import copy
from datetime import datetime
import numpy as np
import matplotlib.pyplot as plt
import scipy.optimize

class DB:

    """ Useful routines to analyse data in DataBase """

    def __init__(self, dbLocation = 'ols.db'):
        self.dbLocation = dbLocation
        self.conn = sqlite3.connect(self.dbLocation, detect_types=sqlite3.PARSE_DECLTYPES)
        self.cursor = self.conn.cursor()

    def __enter__(self):
        return self

    def __exit__(self):
        self.conn.close()
    


    def retrieveFileNames(self, searchFields):
        """ searchFields is dict, e.g. {'lightintensity':900,'darkduration':60} """

        clauses = []
        for k,v in searchFields.items():
            if type(v) == str:
                clauses.append('('+k+' == \''+v+'\')')
            else:
                clauses.append('('+k+' == '+str(v)+')')

        whereClause = ' AND '.join(clauses)

        c = self.cursor
        fnames = []
        for row in c.execute('SELECT DISTINCT(filename) FROM measurements77k WHERE '+whereClause):
            fnames.append(row)

        return(fnames)


    def retrieveDataSets(self, searchFields, 
                         retrieveFields = ['ID', 'expnr', 'spectra', 'rawabs', 'absnorm', 'sampleabs', 'noise'],
                         infoFields = ['date','lightExposure','lightIntensity','lightFrequency','strain','concentration']
                         ):
        """
        retrieves data sets according to the search criteria defined in dict searchFields.

        retrieveFields is a list of database fields stored in the resulting dataSet object

        infoFields are database fields with useful information characterising the whole dataset,
        which are therefore expected to be identical in all entries in the dataset and thus only
        stored as additional information once
        """

        fnames = self.retrieveFileNames(searchFields)

        c = self.cursor
        dataSets = []

        selectClause = ', '.join(retrieveFields + infoFields);

        for fn in fnames:
            d = []
            for row in c.execute('SELECT '+selectClause+' FROM measurements77k WHERE filename like ?', fn):
                d.append(row)
            
            optInfo = {'filename':fn}
            for i in range(len(infoFields)):
                optInfo[infoFields[i]] = d[0][len(retrieveFields) + i]

            dataSets.append(DataSet(d, retrieveFields, optInfo))

        return(dataSets)


class DataSet:

    """ Methods performed on single datasets """

    numericFields = ['ID', 'expnr', 'spectra', 'rawabs', 'absnorm', 'sampleabs', 'noise']

    # TODO :fixed markers to keep consistance, also PREPARE COLOR VECTORS, GENERATORS SUCKS
    mstrain = {'C': 'd', 'N':'o', 'S':'s', 'S/N':'.'}
    mday = {1:'r', 2:'g', 3:'b'}
    mint = {150:5,300:10}
 
    def __init__(self, data, fields, optInfo = {}):
        """ 
        a DataSet object contains all the data in an easily accessible way
        """
        
        for i in range(len(fields)):
            dbData = [data[j][i] for j in range(len(data))]
            if fields[i] in DataSet.numericFields:
                setattr(self, fields[i], np.array(dbData))
            else:
                setattr(self, fields[i], dbData)

        for k,v in optInfo.items():
            setattr(self, k, v)

    def getRatio(self, ps2=685, ps1=715):
        """ by default gets the ratio between the cross sections of PSI and PSII
        """
        self.peakps2 = np.where(self.spectra == ps2)
        self.peakps1 = np.where(self.spectra == ps1)

        ratio = self.rawabs[self.peakps1]/self.rawabs[self.peakps2]

        # assign 0 and 1 to referenca data for plotting purpose
        if self.lightFrequency == 'white':
            coordinate = 2
        elif self.lightFrequency == 'dark':
            coordinate = 1
        else:
            coordinate = int(self.lightFrequency)

        ratios = [str(self.strain), self.lightIntensity, coordinate, float(ratio)]

        return ratios

    def getRatioRef(self ,ps2=685, ps1=715):
        self.peakps2 = np.where(self.spectra == ps2)
        self.peakps1 = np.where(self.spectra == ps1)

        ratios = np.array([]).reshape(0, 4)

        if self.lightFrequency == 'white' or self.lightFrequency == 'dark':
            ratio = self.absnorm[self.peakps1]/self.absnorm[self.peakps2]

            ratios = [str(self.strain), self.lightIntensity, len(str(self.lightFrequency)), float(self.absnorm[self.peakps1]/self.absnorm[self.peakps2])]

        return ratios


    def plotAbs(self, version='max', leg_vis=True):

        labels = []
        if version == 'max':
            plt.plot(self.spectra, self.absnorm)
        elif version.startswith('ps2'):
            plt.plot(self.spectra, self.rawabs/max(self.rawabs[np.where(self.spectra < 695)]))
        elif version.startswith('ps1'):
            plt.plot(self.spectra, self.rawabs/max(self.rawabs[np.where(self.spectra == 715)]))

        labels.append(str(self.lightFrequency)+' '+str(self.strain))

        plt.legend(labels, ncol=4, loc='upper center',
           bbox_to_anchor=[0.5, 1.1],
           columnspacing=1.0, labelspacing=0.0,
           handletextpad=0.0, handlelength=1.5,
           fancybox=True, shadow=True)

        # while plotting whoel data set worth supressing the legend display
        ax = plt.gca()
        ax.get_legend().set_visible(leg_vis)

        plt.title('77K spectra measurement of experiment on '+ str(self.date))
        plt.xlabel('wavelengths [nm]')
        plt.ylabel('abs normalized to ' + version)


    def plotRatio(self, ps2=685, ps1=715, typeofmarker = 'strain', description = 'None'):

        self.peakps2 = np.where(self.spectra == ps2)
        self.peakps1 = np.where(self.spectra == ps1)

        t_marker = typeofmarker

        # reference

        if self.lightFrequency != 'white' and self.lightFrequency != 'dark':
            ratio = self.absnorm[self.peakps1]/self.absnorm[self.peakps2]

            # if t_marker == 'strain':
            #     mark = DataSet.marker[str(self.strain)]
            #     plt.plot(self.lightFrequency, ratio, marker=DataSet.marker[str(self.strain)], ms = 10,
            #               label=str(self.strain)+' '+self.lightFrequency+' '+str(self.lightIntensity)+' Day:'+str(self.expnr[0]))
            # elif t_marker == 'day':
            #     mark = DataSet.marked[self.expnr[0]]
            #     col = DataSet.color[self.expnr[0]]
            #     plt.plot(self.lightFrequency, ratio, marker=mark, ms = 10, c=col,
            #               label=str(self.strain)+' '+self.lightFrequency+' '+str(self.lightIntensity)+' Day:'+str(self.expnr[0]))

            plt.plot(self.lightFrequency, self.absnorm[self.peakps1]/self.absnorm[self.peakps2], marker=DataSet.mstrain[str(self.strain)],
                     fillstyle=u'left', label=str(self.strain)+' '+self.lightFrequency+' '+str(self.lightIntensity))

        if description == 'True':
            plt.title('State transitions analysis')
            plt.xlabel('wavelengths [nm]')
            plt.ylabel('F715/F685')

            return ratio


    def plotRatioRef(self, ps2=685, ps1=715, typeofmarker = 'strain'):

            self.peakps2 = np.where(self.spectra == ps2)
            self.peakps1 = np.where(self.spectra == ps1)

            t_marker = typeofmarker

            # reference

            if self.lightFrequency == 'white' or self.lightFrequency == 'dark':
                ratio = self.absnorm[self.peakps1]/self.absnorm[self.peakps2]
                mark = DataSet.marker[str(self.strain)]
                plt.plot(1, ratio, marker=mark, ms = 10,
                              label=str(self.strain)+' '+self.lightFrequency+' '+str(self.lightIntensity)+' Day:'+str(self.expnr[0]))
                plt.xlim([0,2])
                plt.title('Reference values')
                return ratio



