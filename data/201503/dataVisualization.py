__author__ = 'anna'
import numpy as np
import dataAnalysis
import matplotlib.pyplot as plt
from matplotlib import lines

db = dataAnalysis.DB()

# ================================================================== #
# Plot the quality of data obtained through the 77 K #
# ================================================================== #
nrexp = 4
ds = {}
for i in range(1, nrexp+1):
    ds[i] = db.retrieveDataSets({'expnr':i})

# Plot figures below each other

fig = plt.figure()
for i in range(1,len(ds)+1):
    plt.subplot(nrexp,1,i)
    data = ds[i]
    for i in range(len(data)):
        data[i].plotAbs('ps2',False)

fig.subplots_adjust(hspace=.5)
plt.suptitle('Visual quality of the 77K emission measurements')

# ================================================================================================================== #
#                           Krwia i potem wymeczone ponowne wynalezienie kola                                        #
#                              Plot the F715/F685 for each experiment separetly                                      #
# ================================================================================================================== #
reload(dataAnalysis)

db = dataAnalysis.DB()

# standard legend for strains
mstrain = {'C': 'd', 'N':'o', 'S':'s', 'S/N':'v'}
cstrain = {'C': 'r', 'N':'b', 'S':'k', 'S/N':'m'}



for i in range(1,5):

    ds = db.retrieveDataSets({'expnr':i})

    labels = []


    R = np.array([]).reshape(0, 4)
    for i in range(len(ds)):
        R = np.vstack([R,ds[i].getRatio()])

    # get unique names by which I will group data sets

    figure()
    for i in range(len(R)):
        plt.plot(int(R[i][2]), float(R[i][3]), color=cstrain[R[i][0]], marker=mstrain[R[i][0]], linestyle = '--')

    for i in range(len(mstrain.keys())):
        # WORKAROUND!
        index = where(R[:,0] == mstrain.keys()[i]) # returns indexes of rows that contain data for wild type
        new = R[index]    # new small array

        sortind = argsort(R[index,2])

        # add the line to connect between the same specie BUT you need to sort them in the order as they connect step wise
        line = lines.Line2D(new[sortind,2], new[sortind,3],linestyle='dashed',color = cstrain[new[i][0]], label = str(new[i][0]))
        plt.axes().add_line(line)

    plt.title(r'77K measurements for all strains exposed on the '+str(ds[0].date), fontsize=16)
    plt.xlabel('wavelengths [nm]', fontsize=16)
    plt.ylabel('F715/F685',fontsize=16)
    ax = gca()
    handles, labels = ax.get_legend_handles_labels()
    labels, handles = zip(*sorted(zip(labels[:4], handles[:4]), key=lambda t: t[0]))
    ax.legend(handles, labels, ncol=4, loc='upper center',title="Strains",
               bbox_to_anchor=[0.5, 1],
               columnspacing=1.0, labelspacing=0.0,
               handletextpad=0.0, handlelength=1.5,
               fancybox=True, shadow=True)

# ================================================================================================================== #


# ================================================================================================================== #
#                               Krwia i potem wymeczone ponowne wynalezienie kola                                    #
#                            Plot the F715/F685 for WT under three different light                                   #
# ================================================================================================================== #
import matplotlib.gridspec as gridspec
from dataAnalysis import DB

class PlotOLS(DB):

    def __init__(self, type):
        # standard legend for strains
        self.mstrain = {'C': 'd', 'N':'o', 'S':'s', 'S/N':'v'}
        self.code = {'C': 'wild type', 'N':'npq4 mutant', 'S':'stt7-9 mutant', 'S/N':'stt7npq4 mutant'}
        self.cstrain = {'C': 'g', 'N':'b', 'S':'y', 'S/N':'m'}

        self.msize = {'90':5, '150':10, '300':15}
        self.cintensity = {'90':'b', '150':'k', '300':'r', 'white':'y', 'dark':'k'}


        # order of data is 90 stored in exp 4, 150 in 2 and 300 in 3
        self.lightorder = [4,2,3]

    def getRainbowBar(self, axis):
        cmap = matplotlib.cm.nipy_spectral
        cb1 = matplotlib.colorbar.ColorbarBase(axis, cmap = cmap, orientation='horizontal')
        plt.setp(axis.get_xticklabels(), visible=False)
        plt.tick_params(
                axis='y',          # changes apply to the y-axis
                left='on',        # ticks along the left edge are off
                top='off',         # ticks along the top edge are off
                bottom='off',
                labelleft='off')    # labels along the bottom edge are off



    def compareLights(self, strain):
        self.strain = strain

        fig = figure()
        fig.set_size_inches(8, 3)
        gs = gridspec.GridSpec(2, 2,
                           width_ratios=[8,1],
                           height_ratios=[20,1]
                           )
        ax1 = plt.subplot(gs[0])

        # loop over light intensities stored in different files
        for i in self.lightorder:

            ds = db.retrieveDataSets({'strain':self.strain, 'expnr':i})

            labels = []

            R = np.array([]).reshape(0, 4)
            for i in range(len(ds)):
                R = np.vstack([R,ds[i].getRatio()])

            # get unique names by which I will group data sets

            for j in range(len(R)):
                sortind = argsort(R[:,2])
                ax1 = plt.plot(R[sortind,2], R[sortind,3], color=self.cintensity[R[j][1]], marker=self.mstrain[R[j][0]],
                         linestyle = '--', label = str(ds[j].lightIntensity) if j == 0 else "")
                print ds[i].lightIntensity

            # plt.title(r'exposed to different light intensities', fontsize=16)
            plt.xlabel('wavelengths [nm]', fontsize=16)
            plt.ylabel('F715/F685',fontsize=20, fontweight='bold')
            ax1 = gca()

            # Plot reference data
            ax2 = plt.subplot(gs[1], sharey=ax1)

            for ii in self.lightorder:

                ds = db.retrieveDataSets({'strain':str(j), 'expnr':ii})

                labels = []
                ax2.set_xlim([3,6])

                plt.title(r'Reference values', fontsize=16)
                plt.xticks([4,5],['dark', 'white'], fontsize=16, fontweight='bold', rotation='vertical') # mark the labels
                plt.tick_params(
                    axis='y',          # changes apply to the y-axis
                    left='on',        # ticks along the left edge are off
                    top='off',         # ticks along the top edge are off
                    labelleft='off')    # labels along the bottom edge are off

                Rf = np.array([]).reshape(0, 4)
                for i in range(len(ds)):
                    Rf = np.vstack([Rf,ds[i].getRatioRef()])
                    # Rf stores on the third position length of the label
                    # len('white') = 5
                    # len('dark') = 4
                ax2 = plt.plot(Rf[:,2], Rf[:,3], color=self.cintensity[Rf[0][1]], marker=self.mstrain[Rf[0][0]], linestyle='None',
                                 label = str(ds[ii].lightIntensity))

                ax2 = gca()
                ax2.legend(ncol=3, loc='upper center',title="Light intensity",
                           bbox_to_anchor=[0.5, 1],
                           columnspacing=1.0, labelspacing=0.0,
                           handletextpad=0.0, handlelength=1.5,
                           fancybox=True, shadow=True, fontsize=16)
                ax2.legend().draggable()

        plt.suptitle(r'77K measurements for the '+str(self.code[ds[i].strain])+' $Chlamydomonas$', fontsize=24, fontweight='bold')

        ax3 = plt.subplot(gs[2])
        self.getRainbowBar(ax3)



        ##################################
# loop over strains
for j in mstrain.keys():

    # set the grid so the references can be plotted seperately
    fig = figure()
    fig.set_size_inches(8, 3)
    # ax1 = plt.subplot2grid((1,6),(0,0),colspan = 5)

    gs = gridspec.GridSpec(2, 2,
                       width_ratios=[12,1],
                       height_ratios=[20,1]
                       )
    ax1 = plt.subplot(gs[0])
    ax1.set_xlim(365,725)
    ax1.set_yticks([0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1])
    ax1.set_yticklabels([0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1], fontsize=16)
    ax1.set_ylim(0.3,1)


    # loop over light intensities stored in different files
    for i in lightorder:

        ds = db.retrieveDataSets({'strain':str(j), 'expnr':i})

        labels = []


        R = np.array([]).reshape(0, 4)
        for i in range(len(ds)):
            R = np.vstack([R,ds[i].getRatio()])

        # get unique names by which I will group data sets

        for i in range(len(R)):
            sortind = argsort(R[:,2])
            ax1 = plt.plot(R[sortind,2], R[sortind,3], color=cintensity[R[i][1]], marker=mstrain[R[i][0]],
                           markersize=10,
                     linestyle = '--', label = str(ds[i].lightIntensity) if i == 0 else "")

       # plt.title(r'exposed to different light intensities', fontsize=16)
        plt.xlabel('wavelengths [nm]', fontsize=20)
        plt.xticks([370, 400, 430, 450, 475, 500, 520, 540, 580, 620, 640, 660, 680, 700, 720], fontsize=20)

        #plt.xticks(([int(R[sortind,2][k]) for k in range(2,len(R))]), fontsize=16)
        plt.ylabel('F715/F685',fontsize=20, fontweight='bold')
        ax1 = gca()
        # ax1.legend(ncol=3, loc='lower center',title="Light intensity",
        #            bbox_to_anchor=[0.5, 1],
        #            columnspacing=1.0, labelspacing=0.0,
        #            handletextpad=0.0, handlelength=1.5,
        #            fancybox=True, shadow=True, visible=False)

    # Plot reference data

    # ax2 = plt.subplot2grid((1,6),(0,5), sharey = ax1)
    ax2 = plt.subplot(gs[1])

    for ii in lightorder:

        ds = db.retrieveDataSets({'strain':str(j), 'expnr':ii})

        labels = []
        ax2.set_xlim([3,6])
        ax2.set_ylim([0.3,1])

        plt.title(r'Reference values', fontsize=16)
        plt.xticks([4,5],['dark', 'white'], fontsize=16, fontweight='bold', rotation='vertical') # mark the labels
        plt.tick_params(
            axis='y',          # changes apply to the y-axis
            left='on',        # ticks along the left edge are off
            top='off',         # ticks along the top edge are off
            labelleft='off')    # labels along the bottom edge are off

        Rf = np.array([]).reshape(0, 4)
        for i in range(len(ds)):
            Rf = np.vstack([Rf,ds[i].getRatioRef()])
            # Rf stores on the third position length of the label
            # len('white') = 5
            # len('dark') = 4
        ax2 = plt.plot(Rf[:,2], Rf[:,3], color=cintensity[Rf[0][1]], marker=mstrain[Rf[0][0]], markersize=10, linestyle='None',
                         label = str(ds[ii].lightIntensity))

        ax2 = gca()
        ax2.legend(ncol=3, loc='upper center',title="Light intensity",
                   bbox_to_anchor=[0.5, 1],
                   columnspacing=1.0, labelspacing=0.0,
                   handletextpad=0.0, handlelength=1.5,
                   fancybox=True, shadow=True, fontsize=30)
        ax2.legend().draggable()




    ax3 = plt.subplot(gs[2])
    cmap = matplotlib.cm.nipy_spectral
    cb1 = matplotlib.colorbar.ColorbarBase(ax3, cmap = cmap, orientation='horizontal')
    plt.setp(ax3.get_xticklabels(), visible=False)
    plt.tick_params(
            axis='y',          # changes apply to the y-axis
            left='on',        # ticks along the left edge are off
            top='off',         # ticks along the top edge are off
            bottom='off',
            labelleft='off')    # labels along the bottom edge are off

    plt.tight_layout()
    plt.suptitle(r'77K measurements for the '+str(code[ds[i].strain])+' $Chlamydomonas$', fontsize=24, fontweight='bold')

# ================================================================================================================== #


#
#
#
#
# R[where(R[:,0]=='C'),2] # wavelengths. not in the order!
#
# plt.plot(R[index,2], R[index,3], color= cstrain['C'], marker=mstrain['C'], linestyle = '--')
#
#
# ax = gca()
#
# # order teh handles
# handles, labels = ax.get_legend_handles_labels()
# labels, handles = zip(*sorted(zip(labels[:4], handles[:4]), key=lambda t: t[0]))
#
# ax.legend(handles, labels, ncol=4, loc='upper center',
#            bbox_to_anchor=[0.5, 1.1],
#            columnspacing=1.0, labelspacing=0.0,
#            handletextpad=0.0, handlelength=1.5,
#            fancybox=True, shadow=True)
# plt.title(r'State transitions for WT($\diamond$) and NPQ4($\bullet$)', fontsize=10)
# plt.xlabel('wavelengths [nm]', fontsize=10)
# plt.ylabel('F715/F685',fontsize=10)
# plt.show()
#
#
# ax = gca()
#
# handles, labels = ax.get_legend_handles_labels()
# # sort both labels and handles by labels
# labels, handles = zip(*sorted(zip(labels, handles), key=lambda t: t[0]))
# ax.legend(handles, labels,ncol=4, loc='upper center',
#            bbox_to_anchor=[0.5, 1.1],
#            columnspacing=1.0, labelspacing=0.0,
#            handletextpad=0.0, handlelength=1.5,
#            fancybox=True, shadow=True)
# plt.title('State transitions analysis')
# plt.xlabel('wavelengths [nm]')
# plt.ylabel('F715/F685')
# plt.show()
# ```
# results in plot like below:
# ![c_n_300](https://git.hhu.de/matuszyn/experiments/uploads/e33273b360478789fcf739237b0769a4/c_n_300.png)
#
# Observations: quenching in the orange region!
#
# To connect points of specific cathegory:
# ```
# x = {} # an empty dictionary that will store the data
# c = np.array([]).reshape(0, 2)
# n = np.array([]).reshape(0, 2)
# for i in range(len(ds)):
#     if ds[i].strain == 'C':
#         value = ds[i].plotRatio(typeofmarker='strain')
#         c = np.vstack([c, [ds[i].lightFrequency, value[0]]])
#     elif ds[i].strain == 'N':
#         value = ds[i].plotRatio(typeofmarker='strain')
#         n = np.vstack([n, [ds[i].lightFrequency, value[0]]])
#
# plt.plot(c[0:],c[:1], 'r')
#
#
# for i in range(len(ds)):
#         ds[i].plotRatioRef(typeofmarker='strain')
