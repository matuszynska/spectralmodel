# -*- coding: utf-8 -*-
"""
Created on Tue May 12 21:23:44 2015

@author: anna
"""

import numpy as np
import matplotlib.pyplot as plt
import os
import sys

class ColdFluoData():



    def __init__(self, filename, day=3):

        self.day = day

        if day == 1:
            self.nr = 22
            strain_label = ['C','N','S','S/N']*self.nr
            wavelengths = (370, 400, 430, 440, 450, 460, 475, 485, 500, 540, 580, 620, 640, 650, 660,
               680, 670,
               690, 700, 720, 'white', 'dark', )

        elif day == 2:
            self.nr = 23
            strain_label = ['C','N','S','S/N']*self.nr
            wavelengths = ('white', 'dark',
                           475, 370,
                           400, 430, 440, 450, 460, 485,
                           500, 520, 540, 580, 620, 640, 650, 660, 680, 670, 690, 700, 720)

        if day == 3:
            self.nr = 24
            strain_label = ['C','N','S','S/N']*self.nr
            wavelengths = ('white', 'dark', 370, 400, 430, 440, 450, 460, 475,
                           500, 485,
                           520, 540, 560, 580, 620, 640, 650, 660, 680, 670, 690, 700, 720)

        spectrum = map(lambda x: [x]*4, wavelengths)
        # makes a flat list out of the list of lists constructed above
        spec_label = [item for sublist in spectrum for item in sublist]

        label_dict = dict(zip(range(1, self.nr*4+1), zip(spec_label, strain_label)))


        data = np.genfromtxt(filename, dtype=float, delimiter='', names=True)
        spectra = data['A']
        abs = data['B']
        absnorm = data['B']/max(abs)

        # indexing of the experiment: 1 -> 88 from 375 to white to dark (670 and 680 reverted order)
        labelKey = int(filename[7:-4])

        lspec = label_dict[labelKey][0]
        lstrain = label_dict[labelKey][1]

        setattr(self, 'labeldict', label_dict)
        setattr(self, 'spectra', spectra)
        setattr(self, 'abs', absnorm)
        setattr(self, 'ind', labelKey)
        setattr(self, 'spec_label', lspec)
        setattr(self, 'strain_label', lstrain)


    def plotTraces(self, colormap = 1, label=None):
        """ plots time traces of experiments in tlist (array or tuple) """

        if colormap == 1:
            colormap = plt.cm.rainbow
            plt.gca().set_color_cycle([colormap(i) for i in np.linspace(0, 0.9, 22)])

            # set the light spectra gradient above
            x = range(10)
            y = range(10)
            z = [[z]*10 for z in range(10)]
            num_bars = 200


        labels = []
        plt.plot(self.spectra, self.abs)
        labels.append(str(self.spec_label)+' '+str(self.strain_label))

        plt.legend(labels, ncol=4, loc='upper center',
           bbox_to_anchor=[0.5, 1.1],
           columnspacing=1.0, labelspacing=0.0,
           handletextpad=0.0, handlelength=1.5,
           fancybox=True, shadow=True)

        return

class Giovanni(ColdFluoData):

    def __init__(self, path = '.'):
        self.allfiles = os.listdir(path)
        self.path = path

    def plotBy(self, filter='C'):
        labels = []
        colormap = plt.cm.spectral
        plt.gca().set_color_cycle([colormap(i) for i in np.linspace(0, 0.9, 22)])

        for file in self.allfiles:
            current = os.path.join(self.path, file)
            if current[2] == 'D':
                c = ColdFluoData(file)
                if type(filter) == str and filter != 'dark':
                    if c.strain_label == filter:
                        c.plotTraces(0)
                        labels.append(str(c.spec_label)+' '+str(c.strain_label))
                elif filter == 'dark':
                    if c.spec_label == filter:
                        c.plotTraces(0)
                        labels.append(str(c.spec_label)+' '+str(c.strain_label))
                elif type(filter) == int:
                    if c.spec_label == filter:
                        c.plotTraces(0)
                        labels.append(str(c.spec_label)+' '+str(c.strain_label))

        plt.legend(labels, ncol=4, loc='upper center',
                   bbox_to_anchor=[0.5, 1.1],
                   columnspacing=1.0, labelspacing=0.0,
                   handletextpad=0.0, handlelength=1.5,
                   fancybox=True, shadow=True)

    def plotPeaks(self, ps2 = 685, ps1 = 715):

        mark = {'C':'d','N':'o','S':'s','S/N':'.'}

        for file in self.allfiles:
            current = os.path.join(self.path, file)

            abspeaks = []
            labels = []

            if current[2] == 'D':
                c = ColdFluoData(file)
                ind2 = np.where(c.spectra == ps2)
                ind1 = np.where(c.spectra == ps1)

                if type(c.spec_label) == int:
                    plt.plot(c.spec_label, c.abs[ind2]/c.abs[ind1], marker=mark[c.strain_label], linestyle='--') #, linestyle='none')

    def plotPeaksConnected(self, ps2 = 685, ps1 = 715):

mark = {'C':'d','N':'-o','S':'s','S/N':'.'}

results = {}
vectors = {}

for filename in allfiles:
    data = np.genfromtxt(filename, dtype=float, delimiter='', names=True)



    spectra = data['A']
    abs = data['B']
    absnorm = data['B']/max(abs)

    # indexing of the experiment: 1 -> 88 from 375 to white to dark (670 and 680 reverted order)
    labelKey = int(filename[7:-4])

    lspec = label_dict[labelKey][0]

    ind2 = np.where(spectra == ps2)[0][0]
    ind1 = np.where(spectra == ps1)[0][0]

    lstrain = label_dict[labelKey][1]

    results[labelKey] = {'freq': lspec, 'strain': lstrain, 'spectraps2': ind2, 'spectraps1': ind1, 'abs':absnorm}

    if results[i]['freq'] != 'white' or results[i]['freq'] != 'dark':

        for i in results.keys():
            vectors[results[i]['strain']] = (results[i]['freq'], results[i]['spectraps1']/results[i]['spectraps2'])


        plt.plot(results spec_label, c.abs[ind2]/c.abs[ind1], marker=mark[c.strain_label], linestyle='--') #, linestyle='none')
