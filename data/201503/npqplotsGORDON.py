__author__ = 'anna'


# ugly but working
p = plotols.PlotOLS(DB)

# First upper plot for experiments done for 150 ue (AS 77 K)
n = plotols.NPQData('FluorCam/150513_150uE_spectral_excitation_qE.TXT',format='d2')

# set the environment with my brilliant setting
fig, (ax1,ax2,ax3,ax4) = p.setAxes()
sp=[370,400,430,440,450,460,475,485,500,520,540,
                             580,620,640,650,660,670,680,690,700,720]
# calculate the NPQ values
d2 = n.calculateNPQ()

# start with plotting the reference values
ax1.plot(1,d2[0],'dg', markersize = 10, label='wild type')
ax1.plot(2,d2[1],'dg', markersize = 10)
ax1.plot(1,d2[23],'ob', markersize = 10,label='npq4 mutant')
ax1.plot(2,d2[24],'ob',markersize = 10)
ax1.plot(1,d2[46],'sy', markersize = 10, label='stt7-9 mutant')
ax1.plot(2,d2[47],'sy',markersize = 10)
ax1.plot(1,d2[69],'vm', markersize = 10, label='npq4 stt7-9 mutant')
ax1.plot(2,d2[70],'vm', markersize = 10)


handles, labels = ax1.get_legend_handles_labels()

ax2.legend(handles, labels, ncol=4, loc='center',title="Strains",
            bbox_to_anchor=[0.5, 1],
            columnspacing=1.0, labelspacing=0.0,
            handletextpad=0.0, handlelength=1.5,
            fancybox=True, shadow=True, fontsize=20)


ax2.plot(sp,d2[2:23],'g',marker='d',markersize = 10)
ax2.plot(sp,d2[25:46],'b',marker='o',markersize = 10)
ax2.plot(sp,d2[48:69],'y',marker='s',markersize = 10)
ax2.plot(sp,d2[71:92],'m',marker='v',markersize = 10)

ax2.set_xlabel('wavelengths [nm]', fontsize=16)
ax1.set_ylabel('qE',fontsize=20, fontweight='bold')

plt.suptitle(r'qE measurements for strains exposed to 150 $\mu E m^{-2}s^{-1}$', fontsize=24, fontweight='bold')


# ============================================================================================================= #
fig, (ax1,ax2,ax3,ax4) = p.setAxes()

# 90 results
n90 = plotols.NPQData('FluorCam/150515_90uE_spectral_excitation_qE.TXT',format='d3')
d90 = n90.calculateNPQ()
sp3=[370,400,430,440,450,460,475,485,500,520,540,560,
                             580,620,640,650,660,670,680,690,700,720]

ax1.plot(1,d90[0],'db', markersize = 10, label='90')
ax1.plot(2,d90[1],'db', markersize = 10)
ax2.plot(sp3,d90[2:24],'db', markersize = 10, linestyle = '-')

# 150 results
n = plotols.NPQData('FluorCam/150513_150uE_spectral_excitation_qE.TXT',format='d2')
d2 = n.calculateNPQ()

ax1.plot(1,d2[0],c='0.75', marker='d', markersize = 10, label='150', linestyle='None')
ax1.plot(2,d2[1],c='0.75', marker='d', markersize = 10)
ax2.plot(sp,d2[2:23], c='0.75', marker='d', markersize = 10, linestyle = '-')

# 300 results
n300 = plotols.NPQData('FluorCam/150514_300uE_spectral_excitation_qE.TXT',format='d3')
d300 = n300.calculateNPQ()

ax1.plot(1,d300[0],'dr', markersize = 10, label='300')
ax1.plot(2,d300[1],'dr', markersize = 10)
ax2.plot(sp3,d300[2:24],'dr',markersize = 10, linestyle = '-')

handles, labels = ax1.get_legend_handles_labels()

ax2.legend(handles, labels, ncol=3, loc='best',title="Light intensity",
    bbox_to_anchor=[0.5, 1],
    columnspacing=1.0, labelspacing=0.0,
    handletextpad=0.0, handlelength=1.5,
    fancybox=True, shadow=True, fontsize=20)

ax2.set_xlabel('wavelengths [nm]', fontsize=16)
ax1.set_ylabel('qE',fontsize=20, fontweight='bold')

plt.suptitle(r'qE measurements for wild type $Chlamydomonas$', fontsize=24, fontweight='bold')