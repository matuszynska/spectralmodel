# -*- coding: utf-8 -*-
"""
Created on Tue May 12 21:23:44 2015

@author: oliver
"""

import numpy as np
import matplotlib.pyplot as plt

class NPQData():
    
    metaDataDay1 = {'spectrum':
                        ['light','dark']+[str(x) for x in 
                            [370,400,430,440,450,460,475,485,500,540,
                             580,620,640,650,660,670,680,690,700,720]],
                    'strain':
                        ['wt','npq4','stt7','npq4stt7']
                    }
        
    metaDataDay2 = {'spectrum':
                        ['light','dark']+[str(x) for x in 
                            [370,400,430,440,450,460,475,485,500,520,540,
                             580,620,640,650,660,670,680,690,700,720]],
                    'strain':
                        ['wt','npq4','stt7','npq4stt7']
                    }
        
    metaDataDay3 = {'spectrum':
                        ['light','dark']+[str(x) for x in 
                            [370,400,430,440,450,460,475,485,500,520,540,
                             560,580,620,640,650,660,670,680,690,700,720]],
                    'strain':
                        ['wt','npq4','stt7','npq4stt7']
                    }
        
    def __init__(self,filename,format='d1'):
        #m = np.genfromtxt(filename,delimiter="\t",names=True,skip_header=2)
        m = np.genfromtxt(filename,skip_header=3)
        T = m[:,0]/1e6
        Fraw = m[:,1:]
        
        if format == 'd1':
            lstrain = [s for s in NPQData.metaDataDay1['strain'] for _ in range(len(NPQData.metaDataDay1['spectrum']))]
            lspec = NPQData.metaDataDay1['spectrum'] * 4
            lstrain+=['bg1','bg2','bg3']
            lspec+=['bg1','bg2','bg3']
            F0duration = 5
            FMinterval = [5.1,6]

        elif format == 'd2':
            lstrain = [s for s in NPQData.metaDataDay2['strain'] for _ in range(len(NPQData.metaDataDay2['spectrum']))]
            lspec = NPQData.metaDataDay2['spectrum'] * 4
            lstrain+=['bg1','bg2']
            lspec+=['bg1','bg2']
            F0duration = 10
            FMinterval = [10.3,10.8]
        
        elif format == 'd3':
            lstrain = [s for s in NPQData.metaDataDay3['strain'] for _ in range(len(NPQData.metaDataDay3['spectrum']))]
            lspec = NPQData.metaDataDay3['spectrum'] * 4
            #lstrain+=['bg1','bg2']
            #lspec+=['bg1','bg2']
            F0duration = 10
            FMinterval = [10.3,10.8]
        
        elif format == 'test':
            lstrain = NPQData.metaDataDay1['strain']
            lspec = ['test'] * 4
            F0duration = 5
            FMinterval = [5.3,6]
            
        elif format == 'testd3':
            lstrain = [s for s in NPQData.metaDataDay1['strain'] for _ in (0,1)]
            lspec = ['dark','HL'] * 4
            F0duration = 10
            FMinterval = [10.3,10.8]

        elif format == 'testd4':
            lstrain = NPQData.metaDataDay1['strain']
            lspec = ['dark/UV'] * 4
            F0duration = 10
            FMinterval = [10.3,10.8]

        FM = Fraw[np.where(np.logical_and(T>FMinterval[0],T<FMinterval[1]))].mean(0)
        F = Fraw/FM
        F[np.where(F>1)] = 1
        F0raw = Fraw[T<F0duration,:].mean(0)
        F0 = F[T<F0duration,:].mean(0)
            
        setattr(self,'T',T)
        setattr(self,'Fraw',Fraw)
        setattr(self,'F',F)
        setattr(self,'F0raw',F0raw)
        setattr(self,'F0',F0)
        setattr(self,'FM',FM)
        setattr(self,'spec_label',lspec)
        setattr(self,'strain_label',lstrain)
        
        
    def plotTraces(self,tlist,label=None):
        """ plots time traces of experiments in tlist (array or tuple) """
        
        colormap = plt.cm.rainbow
        plt.gca().set_color_cycle([colormap(i) for i in np.linspace(0, 0.9, len(tlist))])


        plt.plot(self.T,self.F[:,tlist])
        if label:
            if label.startswith('sp'):
                plt.legend([self.spec_label[i] for i in tlist],loc='best')
            elif label.startswith('st'):
                plt.legend([self.strain_label[i] for i in tlist],loc='best')
            elif label.startswith('b'):
                plt.legend([self.strain_label[i]+'_'+self.spec_label[i] for i in tlist],loc='best')
        
        plt.ylim([0,1])
        
    def calculateNPQ(self,maxinterval=[20,30]):
        """ calculate qE from FM' determined from maxinterval """
        FMp = self.F[np.where(np.logical_and(self.T>maxinterval[0],self.T<maxinterval[1]))].max(0)
        setattr(self,'FMp',FMp)
        NPQ = (1-FMp)/FMp
        setattr(self,'NPQ',NPQ)
        return NPQ
        
        