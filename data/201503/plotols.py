__author__ = 'anna'
# ================================================================================================================== #


# ================================================================================================================== #
#                               Krwia i potem wymeczone ponowne wynalezienie kola                                    #
#                            Plot the F715/F685 for WT under three different light                                   #
# ================================================================================================================== #
import numpy as np

import matplotlib
import matplotlib.pyplot as plt
from matplotlib import lines
import matplotlib.gridspec as gridspec
plt.rcParams['legend.numpoints'] = 1

import dataAnalysis
from dataAnalysis import DB


class PlotOLS(DB):

    def __init__(self, dbLocation = 'ols.db'):
        __status__ = "Development"

        self.db = dataAnalysis.DB()

        # standard legend for strains
        self.mstrain = {'C': 'd', 'N':'o', 'S':'s', 'S/N':'v'}
        self.code = {'C': 'wild type', 'N':'npq4 mutant', 'S':'stt7-9 mutant', 'S/N':'stt7npq4 mutant'}
        self.cstrain = {'C': 'g', 'N':'b', 'S':'y', 'S/N':'m'}

        self.msize = {'90':5, '150':10, '300':15}
        self.cintensity = {'90':'b', '150':'0.75', '300':'r'}


        # order of data is 90 stored in exp 4, 150 in 2 and 300 in 3
        self.lightorder = [4,2,3]

    def getRainbowBar(self, axis):
        cmap = matplotlib.cm.nipy_spectral
        cb1 = matplotlib.colorbar.ColorbarBase(axis, cmap = cmap, orientation='horizontal')
        plt.setp(axis.get_xticklabels(), visible=False)
        plt.tick_params(
                axis='y',          # changes apply to the y-axis
                left='on',        # ticks along the left edge are off
                top='off',         # ticks along the top edge are off
                bottom='off',
                labelleft='off')    # labels along the bottom edge are off


    def setAxes(self, x_labels=[370,400,430,440,450,460,475,485,500,520, 540,560, 580,620,640,650,660,670,680,690,700,720]):
        fig = plt.figure()
        fig.set_size_inches(8, 3)
        gs = gridspec.GridSpec(2, 2,
                           width_ratios=[1,10],
                           height_ratios=[20,1]
                           )
        # Plot reference data
        ax1 = plt.subplot(gs[0])
        # Plot the dara
        ax2 = plt.subplot(gs[1], sharey=ax1)
        ax3 = plt.subplot(gs[3])
        ax4 = plt.subplot(gs[3])
        self.getRainbowBar(ax4)

        plt.subplots_adjust(wspace = .01)
        ax1.spines['right'].set_visible(False)
        ax1.yaxis.tick_left()
        ax2.spines['left'].set_visible(False)
        ax2.yaxis.tick_right()
        ax2.tick_params(labelleft='off')
        ax2.set_xticks(x_labels)
        ax2.set_xticklabels(x_labels, fontsize=16, fontweight='bold', rotation='vertical')
        #ax2.set_xtickslabels(fontsize=16)
        ax1.set_xlim(0, 3)
        ax2.set_xlim(365, 725)

        ax1.set_title(r'Reference values', fontsize=16, fontweight='bold')
        ax1.set_xticks([1,2])
        ax1.set_xticklabels(['dark', 'white'], fontsize=16, fontweight='bold', rotation='vertical') # mark the labels

        return fig, (ax1,ax2,ax3,ax4)


    def compareLights(self, strain, new_xticks=[]):
        self.strain = strain
        fig, (ax1,ax2,ax3,ax4) = self.setAxes()

        if new_xticks != []:
            ax2.set_xticks(new_xticks)
            ax2.set_xticklabels(new_xticks)

        # loop over light intensities stored in different files
        for j in self.lightorder:

            self.ds = self.db.retrieveDataSets({'strain':self.strain, 'expnr':j})

            labels = []

            R = np.array([]).reshape(0, 4)
            for i in range(len(self.ds)):
                R = np.vstack([R,self.ds[i].getRatio()])

            # get unique names by which I will group data sets

            for i in range(len(R)):
                sortind = np.argsort(R[:,2])
                ax1.plot(R[sortind,2], R[sortind,3], color=self.cintensity[R[i][1]], marker=self.mstrain[R[i][0]], markersize = 10,
                         linestyle = 'None', label = str(self.ds[i].lightIntensity) if i == 0 else "")
                ax2.plot(R[sortind[2:],2], R[sortind[2:],3], color=self.cintensity[R[i][1]], marker=self.mstrain[R[i][0]], markersize = 10,
                         linestyle = '-') #, label = str(ds[i].lightIntensity) if i == 0 else "")

        handles, labels = ax1.get_legend_handles_labels()

        legenda = ax2.legend(handles, labels, ncol=3, loc='best',title="Light intensity",
            bbox_to_anchor=[0.5, 0.5],
            columnspacing=1.0, labelspacing=0.0,
            handletextpad=0.0, handlelength=1.5,
            fancybox=True, shadow=True, fontsize=20)
        legenda.draggable(state=True)

       # plt.title(r'exposed to different light intensities', fontsize=16)
        ax2.set_xlabel('wavelengths [nm]', fontsize=16)
        ax1.set_ylabel('F715/F685',fontsize=20, fontweight='bold')

        plt.suptitle(r'77K measurements for the '+str(self.code[self.ds[0].strain])+' $Chlamydomonas$', fontsize=24, fontweight='bold')

    def plotExperiment(self, exp, new_xticks=[]):
        self.exp = exp
        fig, (ax1,ax2,ax3,ax4) = self.setAxes()

        if new_xticks != []:
            ax2.set_xticks(new_xticks)
            ax2.set_xticklabels(new_xticks)

        order = ['C','N','S','S/N']

        # loop over light intensities stored in different files
        for j in order:

            self.ds = self.db.retrieveDataSets({'strain':j, 'expnr':self.exp})

            labels = []

            R = np.array([]).reshape(0, 4)
            for i in range(len(self.ds)):
                R = np.vstack([R,self.ds[i].getRatio()])

            # get unique names by which I will group data sets

            for i in range(len(R)):
                sortind = np.argsort(R[:,2])
                ax1.plot(R[sortind,2], R[sortind,3], color=self.cstrain[R[i][0]], marker=self.mstrain[R[i][0]], markersize = 10,
                         linestyle = 'None', label = self.code[str(self.ds[i].strain)] if i == 0 else "")
                ax2.plot(R[sortind[2:],2], R[sortind[2:],3], color=self.cstrain[R[i][0]], marker=self.mstrain[R[i][0]], markersize = 10,
                         linestyle = '-') #, label = str(ds[i].lightIntensity) if i == 0 else "")

        handles, labels = ax1.get_legend_handles_labels()

        ax2.legend(handles, labels, ncol=4, loc='center',title="Strains",
            bbox_to_anchor=[0.5, 1],
            columnspacing=1.0, labelspacing=0.0,
            handletextpad=0.0, handlelength=1.5,
            fancybox=True, shadow=True, fontsize=20)

       # plt.title(r'exposed to different light intensities', fontsize=16)
        ax2.set_xlabel('wavelengths [nm]', fontsize=16)
        ax1.set_ylabel('F715/F685',fontsize=20, fontweight='bold')

        plt.suptitle(r'77K measurements for strains exposed to ' +
                     str(self.ds[0].lightIntensity) + r' $\mu E m^{-2}s^{-1}$', fontsize=24, fontweight='bold')

class NPQData(PlotOLS):

    metaDataDay1 = {'spectrum':
                        ['light','dark']+[str(x) for x in
                            [370,400,430,440,450,460,475,485,500,540,
                             580,620,640,650,660,670,680,690,700,720]],
                    'strain':
                        ['wt','npq4','stt7','npq4stt7']
                    }

    metaDataDay2 = {'spectrum':
                        ['light','dark']+[str(x) for x in
                            [370,400,430,440,450,460,475,485,500,520,540,
                             580,620,640,650,660,670,680,690,700,720]],
                    'strain':
                        ['wt','npq4','stt7','npq4stt7']
                    }

    metaDataDay3 = {'spectrum':
                        ['light','dark']+[str(x) for x in
                            [370,400,430,440,450,460,475,485,500,520,540,
                             560,580,620,640,650,660,670,680,690,700,720]],
                    'strain':
                        ['wt','npq4','stt7','npq4stt7']
                    }

    def __init__(self,filename,format='d1'):
        #m = np.genfromtxt(filename,delimiter="\t",names=True,skip_header=2)
        m = np.genfromtxt(filename,skip_header=3)
        T = m[:,0]/1e6
        Fraw = m[:,1:]

        if format == 'd1':
            lstrain = [s for s in NPQData.metaDataDay1['strain'] for _ in range(len(NPQData.metaDataDay1['spectrum']))]
            lspec = NPQData.metaDataDay1['spectrum'] * 4
            lstrain+=['bg1','bg2','bg3']
            lspec+=['bg1','bg2','bg3']
            F0duration = 5
            FMinterval = [5.1,6]

        elif format == 'd2':
            lstrain = [s for s in NPQData.metaDataDay2['strain'] for _ in range(len(NPQData.metaDataDay2['spectrum']))]
            lspec = NPQData.metaDataDay2['spectrum'] * 4
            lstrain+=['bg1','bg2']
            lspec+=['bg1','bg2']
            F0duration = 10
            FMinterval = [10.3,10.8]

        elif format == 'd3':
            lstrain = [s for s in NPQData.metaDataDay3['strain'] for _ in range(len(NPQData.metaDataDay3['spectrum']))]
            lspec = NPQData.metaDataDay3['spectrum'] * 4
            #lstrain+=['bg1','bg2']
            #lspec+=['bg1','bg2']
            F0duration = 10
            FMinterval = [10.3,10.8]

        elif format == 'test':
            lstrain = NPQData.metaDataDay1['strain']
            lspec = ['test'] * 4
            F0duration = 5
            FMinterval = [5.3,6]

        elif format == 'testd3':
            lstrain = [s for s in NPQData.metaDataDay1['strain'] for _ in (0,1)]
            lspec = ['dark','HL'] * 4
            F0duration = 10
            FMinterval = [10.3,10.8]

        elif format == 'testd4':
            lstrain = NPQData.metaDataDay1['strain']
            lspec = ['dark/UV'] * 4
            F0duration = 10
            FMinterval = [10.3,10.8]

        FM = Fraw[np.where(np.logical_and(T>FMinterval[0],T<FMinterval[1]))].mean(0)
        F = Fraw/FM
        F[np.where(F>1)] = 1
        F0raw = Fraw[T<F0duration,:].mean(0)
        F0 = F[T<F0duration,:].mean(0)

        setattr(self,'T',T)
        setattr(self,'Fraw',Fraw)
        setattr(self,'F',F)
        setattr(self,'F0raw',F0raw)
        setattr(self,'F0',F0)
        setattr(self,'FM',FM)
        setattr(self,'spec_label',lspec)
        setattr(self,'strain_label',lstrain)


    def plotTraces(self,tlist,label=None):
        """ plots time traces of experiments in tlist (array or tuple) """

        colormap = plt.cm.rainbow
        plt.gca().set_color_cycle([colormap(i) for i in np.linspace(0, 0.9, len(tlist))])


        plt.plot(self.T,self.F[:,tlist])
        if label:
            if label.startswith('sp'):
                plt.legend([self.spec_label[i] for i in tlist],loc='best')
            elif label.startswith('st'):
                plt.legend([self.strain_label[i] for i in tlist],loc='best')
            elif label.startswith('b'):
                plt.legend([self.strain_label[i]+'_'+self.spec_label[i] for i in tlist],loc='best')

        plt.ylim([0,1])

    def calculateNPQ(self,maxinterval=[25,35]):
        """ calculate qE from FM' determined from maxinterval """
        FMp = self.F[np.where(np.logical_and(self.T>maxinterval[0],self.T<maxinterval[1]))].max(0)
        setattr(self,'FMp',FMp)
        NPQ = (1-FMp)/FMp
        setattr(self,'NPQ',NPQ)
        return NPQ



