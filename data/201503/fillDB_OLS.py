#!/usr/bin/python
# -*- coding: utf-8 -*-
__author__ = 'anna matuszynska'

# ================================================================ #
# sqlite3 ols.db
# fill the data from the experiments performed between 11 - 17 May 2015 in Okazaki, NIBB
# data base consists of two tables:
# .schema # check the create statement
#
# CREATE TABLE MEASUREMENTS77K(
#     ID INTEGER PRIMARY KEY AUTOINCREMENT,
#     expnr INT,
#     filenr INT,
#     filename VARCHAR(20),
#     date VARCHAR(8),
#     lightExposure INT,
#     lightIntensity INT,
#     lightFrequency VARCHAR(10),
#     strain VARCHAR(10),
#     concentration FLOAT,
#     spectra INT,
#     rawabs FLOAT,
#     absnorm FLOAT,
#     sampleabs FLOAT,
#     noise FLOAT
#     );


import sqlite3
import sys
import numpy as np


#Open connection to the Data Base located in the same working file Soma_data
db = sqlite3.connect('ols.db')

cur = db.cursor()   # get the cursor object

first = True

commarg = sys.argv  # returns list, script name always on the zeroth position

# when having the same directory
dirname, filename = commarg[-1].split('/')  # split the file name into directory and filename

# when files in the same directory
# filename = commarg[-1]

data = np.genfromtxt(dirname+"/"+filename, dtype=float, delimiter='', names=True)

# all files contain only four columns
spectra = data['A']
rawabs = data['B']
sampleabs = data['C']
noise = data['D']

absnorm = rawabs/max(rawabs)

# ====================================================== #
# meta data for experiments performed on the 12 May 2015 #
# ====================================================== #
# strain_label = ['C','N','S','S/N']*22
# wavelengths = (370, 400, 430, 440, 450, 460, 475, 485, 500, 540, 580, 620, 640, 650, 660,
#            680, 670,
#            690, 700, 720, 'white', 'dark')

# ====================================================== #
# meta data for experiments performed on the 13 May 2015 #
#       new wavelength at 520 nm was added               #
# ====================================================== #
# strain_label = ['C','N','S','S/N']*23
# wavelengths = ('white', 'dark', 475, 370,
#                400, 430, 440, 450, 460,
#                485, 500, 520, 540, 580, 620, 640, 650, 660, 670, 680, 690, 700, 720)

# ====================================================== #
# meta data for experiments performed on the 14 May 2015 #
#       new wavelength at 560 nm was added               #
# ====================================================== #
strain_label = ['C','N','S','S/N']*24
wavelengths = ('white', 'dark', 370, 400, 430, 440, 450, 460, 475,
               500, 485,
               520, 540, 560, 580, 620, 640, 650, 660, 670, 680, 690, 700, 720)

frequency = map(lambda x: [x]*4, wavelengths)
# makes a flat list out of the list of lists constructed above
spec_label = [item for sublist in frequency for item in sublist]

# Exp. 1
# label_dict = dict(zip(range(1,89), zip(spec_label, strain_label)))

# Exp. 2 -> missing file number 83
# index_ = range(2,95)
# index_.remove(83)
# label_dict = dict(zip(index_, zip(spec_label, strain_label)))

# Exp. 3
label_dict = dict(zip(range(1,97), zip(spec_label, strain_label)))

# indexing of the experiment: 1 -> 88 from 375 to white to dark (670 and 680 reverted order)
labelKey = int(filename[7:-4])

lspec = label_dict[labelKey][0]
lstrain = label_dict[labelKey][1]

# Insert metadata into table overview that contains all of the experiments
# Exp. 1
# for i in range(len(spectra)):
#     sql = "INSERT INTO MEASUREMENTS77K(expnr, filenr, filename, date, lightExposure, lightIntensity, lightFrequency, strain, concentration, spectra, rawabs, absnorm, sampleabs, noise) VALUES(%d, %d, '%s', '%s', %d, %d, '%s', '%s', %f, %d, %f, %f, %f, %f);"  \
#         % (1, labelKey, filename, '20150512', 3, 300, lspec, lstrain, 5e6, spectra[i], rawabs[i], absnorm[i], sampleabs[i], noise[i])


# Exp. 2
# for i in range(len(spectra)):
#     sql = "INSERT INTO MEASUREMENTS77K(expnr, filenr, filename, date, lightExposure, lightIntensity, lightFrequency, strain, concentration, spectra, rawabs, absnorm, sampleabs, noise) VALUES(%d, %d, '%s', '%s', %d, %d, '%s', '%s', %f, %d, %f, %f, %f, %f);"  \
#         % (2, labelKey, filename, '20150513', 4, 150, lspec, lstrain, 1e7, spectra[i], rawabs[i], absnorm[i], sampleabs[i], noise[i])

# Exp. 3
for i in range(len(spectra)):
    sql = "INSERT INTO MEASUREMENTS77K(expnr, filenr, filename, date, lightExposure, lightIntensity, lightFrequency, strain, concentration, spectra, rawabs, absnorm, sampleabs, noise) VALUES(%d, %d, '%s', '%s', %d, %d, '%s', '%s', %f, %d, %f, %f, %f, %f);"  \
        % (3, labelKey, '3_'+filename, '20150514', 4, 300, lspec, lstrain, 1e7, spectra[i], rawabs[i], absnorm[i], sampleabs[i], noise[i])

    print sql
    #cur.execute(sql)

# Close the connection to the Data Base
db.close()
