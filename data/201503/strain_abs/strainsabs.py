__author__ = 'anna'

# script to plot strains absorption spectra

import numpy as np
import matplotlib.pyplot as plt
import os
import sys

class PigmentComposition():

    def __init__(self, path = '.'):
        self.path = path

        filenames = os.listdir('.')
        files_ = [x for x in filenames if x.endswith('txt')]   # exclude not closed files

        self.files = [x for x in files_ if not x.startswith('BL')]
        self.cellcount = {'C':6.53e6, 'N':2.07e6, 'S':1.53e6, 'SN':1.52e6}
        self.results = {}

        for fn in self.files:
            name, format = fn.split('.')
            self.results[name] = np.genfromtxt(fn, dtype=float, delimiter='')

    def plotAbs(self, strains='all'):

        self.strains = strains

        if self.strains == 'all':
            strain = ['C','N','S','SN']
            cm = ['r','g','b','m']

        plt.figure()

        labels = []
        for s in range(len(strain)):
            frequency = np.hsplit(self.results[strain[s]],2)[0]
            absorption = np.hsplit(self.results[strain[s]],2)[1]

            # ================================================ #
            # normalize by the cell count
            # performed at the Minagawa Lab on the 14 May 2015 #
            norm = self.cellcount[strain[s]]

            plt.plot(frequency, absorption/norm, c=cm[s])
            labels.append(str(strain[s]))

        plt.legend(labels, ncol=4, loc='upper center', bbox_to_anchor=[0.5, 1.1], columnspacing=1.0, labelspacing=0.0,
                   handletextpad=0.0, handlelength=1.5, fancybox=True, shadow=True)
        plt.xlabel('wavelengths [nm]')
        plt.xticks((range(300,850,50)))
        plt.ylabel('Absorption per cells')
        plt.title('Absorption spectra of strains used during the OLS experiment')

        plt.show()


    def relativeComposition():
        #plt.plot(frequency, absorption/norm)
        #labels.append(str(strain))
        print ania


if __name__ == "__main__":
    c = PigmentComposition()
    c.plotAbs()




