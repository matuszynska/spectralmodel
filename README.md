# Aim of the study
Continue the project started at the end of my PhD.To understand how light wavelength affects short term acclimation / **photoprotection** we combine a dual approach basesd on precise spectrometric experiments using Okazaki Large Spectograph Facilities and theoretical simulations obtained through computational simultaions of the photosynthetic electron transport chain.

Read more: [Chapter 6 PhD Thesis](https://gitlab.com/matuszynska/spectralmodel/-/blob/master/images/Chapter6.pdf).

# Experimental methods
Four strains of Chlamydomonas reinhardtii were exposed simultaneously to the monochromatic light using the OLS:

![raw absorption](/images/IMG_20150514_095043.jpg "OLS")

# Implementation
The current main branch includes mergedmodel from 2019.

It is intended to compare the initial results between the full model and the PETC2014. Probably the CBB cycle provides additional level of complexity.

![abs complex](/images/abs_complex.png "abs")

## Activation by light
In order to account for a different absoprtion capacity of PSI and PSII supercomplexes, as well as change in the absorption during state transitions, a new method of calculating the activation rate for PSI and PSII was derived [LI and LII](https://gitlab.com/matuszynska/spectralmodel/-/blob/master/modules.py), based on the specific absorption spectra of eaach of the light harvesting supercomplexes: PSI-LHCI, PSII-LHCII or LHCII 
[get_specific_abs](https://gitlab.com/matuszynska/spectralmodel/-/blob/master/spectra.py)

## Literature
### Must read
[1] the merged model used for this analysis https://onlinelibrary.wiley.com/doi/full/10.1111/ppl.12962
[2] about the 77K measurements
[3] regulation of the PETC https://pubmed.ncbi.nlm.nih.gov/21118674/ 
[4] why light matters, paper by Dimitris that inspired this research https://pubmed.ncbi.nlm.nih.gov/27626383/

### New reads (updated on 14 Oct)
Interesting articles about spectral dependency/fluorometers etc.
[1] https://link.springer.com/article/10.1007/s11120-012-9758-1
[2] https://research.wur.nl/en/publications/spectral-dependence-of-photosynthesis-and-light-absorptance-in-si (?)
