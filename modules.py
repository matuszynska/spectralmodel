import numpy as np 
import spectra

c = spectra.ComplexAbs()

def pqmoiety(PQ, pqtot):
    return pqtot - PQ

def pcmoiety(PC, pctot):
    return pctot - PC


def fdmoiety(Fd, fdtot):
    return fdtot - Fd


def adpmoiety(ATP, atptot):
    return atptot - ATP


def nadpmoiety(NADPH, nadptot):
    return nadptot - NADPH


def lhcmoiety(LHC):
    return 1 - LHC


def xmoiety(Vx, Xtot):
    return Xtot - Vx


def psbsmoiety(Psbs, Psbstot):
    return Psbstot - Psbs


def crosssections(LHC, LHCp, prob_attach, staticAntI, staticAntII):
    """ calculates the cross section of PSII """
    free = (1 - prob_attach) * (1 - staticAntII - staticAntI) * LHCp
    cs1 = staticAntI + prob_attach * (1 - staticAntII - staticAntI) * LHCp
    cs2 = staticAntII + (1 - staticAntII - staticAntI) * LHC

    return [cs2, cs1, free]


def absorptions(wv):
    '''
    return three specific absorption values for:
    [PSII-LHCII supercomplex, PSI-LHCI supercomplex and free LHCII antenna]
    expressed in [nm]
    this rate is used to multiply the cross section and light intensity to provide the amount of absorbed light
    '''
    [abscPSII, abscPSI, abscLHCII] = c.get_specific_abs(wv)
    absPSII = abscPSII[0]
    absPSI = abscPSI[0]
    absLHC = abscLHCII[0]
    return [absPSII, absPSI, absLHC]

def LI(LHCp, staticAntI, staticAntII, prob_attach, pfd, wv):
    l1 = staticAntI * pfd * absorptions(wv)[1] + \
         prob_attach * (1 - staticAntII - staticAntI) * LHCp * pfd * absorptions(wv)[2]
    return l1

def LII(LHC, staticAntI, staticAntII, pfd, wv):
    l2 = staticAntII *  pfd * absorptions(wv)[0] + \
        (1 - staticAntII - staticAntI) * LHC  * pfd * absorptions(wv)[2]
    return l2


def quencher(Psbs, Vx, Psbsp, Zx, y0, y1, y2, y3, kZSat):
    """ 
    co-operatiove 4-state quenching mechanism
    
    Comment:
        seems like this can't reach 1, so I've checked the max value and normalized it
        for i in np.linspace(0,1,100):
            for j in np.linspace(0,1,100):
                a = np.hstack((a, vQuencher4states(p, i, j)))
        max(a)
    """
    ZAnt = Zx / (Zx + kZSat)
    Q = (y0 * Vx * Psbs + y1 * Vx * Psbsp + y2 * ZAnt * Psbsp + y3 * ZAnt * Psbs)#/0.5625
    #Q = (y0 * (1-ZAnt) * Psbs + y1 * (1-ZAnt) * Psbsp + y2 * ZAnt * Psbsp + y3 * ZAnt * Psbs)#/0.5625
    return Q
    

def ps2states(PQ, PQred, Q, L, PSIItot, k2, kF, _kH, Keq_PQred, kPQred, kH0):
    kH = kH0 + _kH * Q
    k3p = kPQred * PQ
    k3m = kPQred * PQred / Keq_PQred
   
    Bs = []

    
    if isinstance(kH, float) and isinstance(PQ, np.ndarray):
        kH = np.repeat(kH, len(PQ))
      
    
    for L, kH, k3p, k3m in zip(L, kH, k3p, k3m):
        M = np.array(
            [
                [-L - k3m, kH + kF, k3p, 0],
                [L, -(kH + kF + k2), 0, 0],
                [0, 0, L, -(kH + kF)],
                [1, 1, 1, 1],
            ]
        )
        A = np.array([0, 0, 0, PSIItot])
        B0, B1, B2, B3 = np.linalg.solve(M, A)
        Bs.append([B0, B1, B2, B3])
    return np.array(Bs).T


def ps1states(PC, PCred, Fd, Fdred, LHC, L, PSItot, kFdred, Keq_FAFd, Keq_PCP700, kPCox):
    """ 
    QSSA calculates open state of PSI
    depends on reduction states of plastocyanin and ferredoxin
    C = [PC], F = [Fd] (ox. forms)
    accepts: light, y as an array of arrays
    returns: array of PSI open
    """
    A1 = PSItot / (1 + L/(kFdred * Fd) + (1 + Fdred/(Keq_FAFd * Fd))
                      * (PC/(Keq_PCP700 * PCred)
                         + L/(kPCox * PCred))
    )
    return A1


def fluorescence(Q, B0, B2, ps2cs, k2, kF, kH, kH0):
    fluo = (ps2cs * kF * B0) / (kF + k2 + kH*Q) + (ps2cs * kF * B2) / (kF + kH*Q)
    return fluo


def calculate_pH(x):
    return (-np.log(x*(2.5e-4))/np.log(10))


def Pimoiety(PGA,BPGA,GAP,DHAP,FBP,F6P,G6P,G1P,SBP,S7P,E4P,X5P,R5P,RUBP,RU5P,ATP, Cp):
    return Cp - (PGA + 2*BPGA + GAP + DHAP + 2*FBP + F6P + G6P + G1P + 2*SBP + S7P + E4P + X5P + R5P + 2*RUBP + RU5P + ATP)


def Nmoiety(Pi, PGA, GAP, DHAP, Kpxt, Pext, Kpi, Kpga, Kgap, Kdhap):
    """Used several times to calculate the rate of vPGA, vGAP and vDHAP"""
    return 1+(1+(Kpxt/Pext))*((Pi/Kpi)
               +(PGA/Kpga)
               +(GAP/Kgap)
               +(DHAP/Kdhap))

