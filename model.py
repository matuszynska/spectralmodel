from modelbase.ode import Model
from parameters import parameters
from modules import *
from reactionrates import *
import numpy as np

model = Model(parameters=parameters)
model.add_compounds([
        "PQ",  # oxidised plastoquinone
        "PC",  # oxidised plastocyan
        "Fd",  # oxidised ferrodoxin
        "ATP",  # stromal concentration of ATP
        "NADPH",  # stromal concentration of NADPH
        "H",  # lumenal protons
        "LHC",#,  # non-phosphorylated antenna
        "Psbs", # PsBs
        "Vx",  #vioolaxathin relative concentration
        "PGA", 
        'BPGA', 
        'GAP', 
        'DHAP', 
        'FBP', 
        'F6P', 
        'G6P', 
        'G1P', 
        'SBP', 
        'S7P', 
        'E4P', 
        'X5P', 
        'R5P', 
        'RUBP', 
        'RU5P']
)

def calculate_pHstroma(x):
    return (-np.log(x*(3.2e-5))/np.log(10))    

# ====================================================================== #
# Composed parameters #
model.add_derived_parameter(
    parameter_name="RT", function=lambda r, t: r * t, parameters=["R", "T"]
)

model.add_derived_parameter(
    parameter_name="dG_pH", function=lambda r, t: np.log(10) * r * t, parameters=["R", "T"]
)

model.add_derived_parameter(
    parameter_name="Hstroma", function=lambda pHstroma: 3.2e4*10**(-pHstroma), parameters=["pHstroma"]
)

model.add_derived_parameter(
    parameter_name="kProtonation", function=lambda Hstroma: 4e-3 / Hstroma, parameters=["Hstroma"]
)

# FIX ME: we used two different definitions for H_stroma (!). This one is Nima's
model.add_derived_parameter(
    parameter_name="H_stroma", function=lambda pHstroma: (10.0**((-1.0)*pHstroma))*1000.0, parameters=["pHstroma"]
)

model.add_derived_parameter(
    parameter_name="Keq_PQred", function=keq_PQred, parameters=["E0_QA", "F", "E0_PQ", "pHstroma", "dG_pH", "RT"]
)

model.add_derived_parameter(
    parameter_name="Keq_cyc", function=Keq_cyc, parameters=["E0_Fd", "F", "E0_PQ", "pHstroma", "dG_pH", "RT"]
)              

model.add_derived_parameter(
    parameter_name="Keq_FAFd", function=Keq_FAFd, parameters=["E0_FA", "F", "E0_Fd", "RT"]
)               

model.add_derived_parameter(
    parameter_name="Keq_PCP700", function=Keq_PCP700, parameters=["E0_PC", "F", "E0_P700", "RT"]
)               

model.add_derived_parameter(
    parameter_name="Keq_FNR", function=Keq_FNR, parameters=["E0_Fd", "F", "E0_NADP", "pHstroma", "dG_pH", "RT"]
) 

model.add_algebraic_module(
    module_name="pq_alm",
    function=pqmoiety,
    compounds=["PQ"],
    derived_compounds=["PQred"],
    modifiers=None,
    parameters=["PQtot"],
)


model.add_algebraic_module(
    module_name="pc_alm",
    function=pcmoiety,
    compounds=["PC"],
    derived_compounds=["PCred"],
    parameters=["PCtot"],
)

model.add_algebraic_module(
    module_name="fd_alm",
    function=fdmoiety,
    compounds=["Fd"],
    derived_compounds=["Fdred"],
    parameters=["Fdtot"],
)

model.add_algebraic_module(
    module_name="adp_alm",
    function=adpmoiety,
    compounds=["ATP"],
    derived_compounds=["ADP"],
    parameters=["APtot"],
)

model.add_algebraic_module(
    module_name="nadp_alm",
    function=nadpmoiety,
    compounds=["NADPH"],
    derived_compounds=["NADP"],
    modifiers=None,
    parameters=["NADPtot"],
)

model.add_algebraic_module(
    module_name="lhc_alm",
    function=lhcmoiety,
    compounds=["LHC"],
    derived_compounds=["LHCp"]
)

model.add_algebraic_module(
    module_name="xantophylls_alm",
    function=xmoiety,
    compounds=["Vx"],
    derived_compounds=["Zx"],
    parameters=['Xtot']
)

model.add_algebraic_module(
    module_name="psbs_alm",
    function=psbsmoiety,
    compounds=["Psbs"],
    derived_compounds=["Psbsp"],
    parameters=['Psbstot']
)

model.add_algebraic_module(
    module_name="crosssections",
    function=crosssections,
    compounds=["LHC", "LHCp"],
    derived_compounds=["ps2cs", "ps1cs", "free"],
    modifiers=None,
    parameters=["prob_attach", "staticAntI", "staticAntII"],
)

model.add_algebraic_module(
    module_name="quencher",
    function=quencher,
    compounds=["Psbs", "Vx", "Psbsp", "Zx"],
    derived_compounds=["Q"],
    modifiers=None,
    parameters=["gamma0", "gamma1", "gamma2", "gamma3", "kZSat"],
)

model.add_algebraic_module(
    module_name="light_ps1",
    function=LI,
    compounds=["LHCp"],
    derived_compounds=["LI"],
    parameters=["staticAntI", "staticAntII", "prob_attach", "pfd", "wv"],
)

model.add_algebraic_module(
    module_name="light_ps2",
    function=LII,
    compounds=["LHC"],
    derived_compounds=["LII"],
    parameters=["staticAntI", "staticAntII", "pfd", "wv"],
)

model.add_algebraic_module(
    module_name="ps2states",
    function=ps2states,
    compounds=["PQ", "PQred", "Q", "LII"],
    derived_compounds=["B0", "B1", "B2", "B3"],
    parameters=["PSIItot", "k2", "kF", "kH", "Keq_PQred", "kPQred", "kH0"],
)

model.add_algebraic_module(
    module_name="ps1states",
    function=ps1states,
    compounds=["PC", "PCred", "Fd", "Fdred", "LHC", "LI"],
    derived_compounds=["A1"],
    parameters=["PSItot", "kFdred", "Keq_FAFd", "Keq_PCP700", "kPCox"],
)

model.add_algebraic_module(
    module_name="fluorescence",
    function=fluorescence,
    compounds=["Q", "B0", "B2", "ps2cs"],
    derived_compounds=["Fluo"],
    modifiers=None,
    parameters=["k2","kF", "kH", "kH0"]
)

model.add_algebraic_module(
    module_name="calculate_pH",
    function=calculate_pH,
    compounds=["H"],
    derived_compounds=["pH"],
    modifiers=None
)

model.add_algebraic_module(
    module_name="pi_alm",
    function=Pimoiety,
    compounds=["PGA","BPGA","GAP","DHAP","FBP","F6P","G6P","G1P","SBP","S7P","E4P","X5P","R5P","RUBP","RU5P","ATP"],
    derived_compounds=["Pi"],
    parameters=["Cp"],
    modifiers=None
)

model.add_algebraic_module(
    module_name="n_alm",
    function=Nmoiety,
    compounds=["Pi","PGA","GAP","DHAP"],
    derived_compounds=["N"],
    parameters=["Kpxt", "Pext", "Kpi", "Kpga", "Kgap", "Kdhap"],
    modifiers=None
)

# Rate of electron flow through the photosystems.
model.add_reaction(
    rate_name='vPS2',
    function=vPS2,
    stoichiometry= {"PQ":-1, "H": 2/model.get_parameter("bH")},
    dynamic_variables=["B1"],
    parameters=["k2"],
    reversible=True,
)

model.add_reaction(
    rate_name='vPS1',
    function=vPS1,
    stoichiometry={"Fd": -1, "PC": 1},
    dynamic_variables=["A1", "LI"],
    reversible=True,
)


model.add_reaction(
    rate_name='vPTOX',
    function=vPTOX,
    stoichiometry= {"PQ": 1},
    modifiers=["PQred", "time"],
    parameters=["kPTOX", "ox", "O2ext", "kNDH", "Ton", "Toff"],
    reversible=False,
)

model.add_reaction(
    rate_name='vNDH',
    function=vNDH,
    stoichiometry=  {"PQ":-1},
    modifiers=["time"],
    parameters=["ox", "O2ext", "kNDH", "Ton", "Toff"],
    reversible=False,
)

model.add_reaction(
    rate_name='vB6f',
    function=vB6f,
    stoichiometry={"PC": -2, "PQ": 1, "H": 4/100},
    modifiers=["PQred", "PCred", "pH"],
    parameters=["kCytb6f", "F", "E0_PQ", "E0_PC", "pHstroma", "RT", "dG_pH"],
    reversible=True,
)

model.add_reaction(
    rate_name='vCyc',
    function=vCyc,
    stoichiometry=  {"PQ": -1, "Fd": 2},
    dynamic_variables=["PQ","Fdred"],
    parameters=["kcyc"],
    reversible=True,
)

model.add_reaction(
    rate_name='vFNR',
    function=vFNR,
    stoichiometry=  {"Fd": 2, "NADPH": 1*model.get_parameter('convf')},
    dynamic_variables=["Fd", "Fdred", "NADPH", "NADP"],
    parameters=["KM_FNR_F", "KM_FNR_N", "EFNR", "kcatFNR", "Keq_FNR", "convf"],
    reversible=False,
)
   
model.add_reaction(
    rate_name='vLeak',
    function=vLeak,
    stoichiometry= {"H": -1/model.get_parameter("bH")},
    modifiers=None,
    parameters=["kLeak", "pHstroma"],
    reversible=False,
)

model.add_reaction(
    rate_name='vSt12',
    function=vSt12,
    stoichiometry= {"LHC": -1},
    modifiers=["PQ"],
    parameters=["kStt7", "PQtot", "KM_ST", "n_ST"],
    reversible=False,
)

model.add_reaction(
    rate_name='vSt21',
    function=vSt21,
    stoichiometry= {"LHC": 1},
    dynamic_variables=["LHCp"],
    parameters=["kPph1"],
    reversible=False,
)

model.add_reaction(
    rate_name='vATPsynthase',
    function=vATPsynthase,
    stoichiometry= {"ATP": 1*model.get_parameter('convf'), "H": -model.get_parameter('HPR')/model.get_parameter('bH')},
    dynamic_variables=["ATP","ADP","pH"],
    parameters=["kATPsynth","DeltaG0_ATP","dG_pH","HPR","pHstroma","Pi_mol","RT","convf"],
    reversible=True,
)

model.add_reaction(
    rate_name='vDeepox',
    function=vDeepox,
    stoichiometry= {"Vx": -1},
    modifiers=["H"],
    parameters=["kHillX", "kDeepoxV", "kphSat"],
    reversible=False,
)

model.add_reaction(
    rate_name='vEpox',
    function=vEpox,
    stoichiometry= {"Vx": 1},
    dynamic_variables=["Zx"],
    parameters=["kEpoxZ"],
    reversible=True,
)

model.add_reaction(
    rate_name='vLhcprotonation',
    function=vLhcprotonation,
    stoichiometry= {"Psbs": -1},
    modifiers=["H"],
    parameters=["kHillL", "kProtonationL", "kphSatLHC"],
    reversible=False,
)

model.add_reaction(
    rate_name='vLhcdeprotonation',
    function=vLhcdeprotonation,
    stoichiometry= {"Psbs": 1},
    dynamic_variables=["Psbsp"],
    parameters=["kDeprotonation"],
    reversible=True,
)

""" Calvin-Benson-Bassham Cycle Reaction Rates """
model.add_reaction(
    rate_name='vRuBisCO',
    function=v1,
    stoichiometry= {"RUBP": -1, "PGA": 2},
    dynamic_variables=["RUBP", "PGA", "FBP", "SBP", "Pi", "NADPH"],
    parameters=["V1", "CO2", "Km1", "Ki11", "Ki12", "Ki13", "Ki14", "Ki15", "KmCO2"],
    reversible=True,
)

model.add_reaction(
    rate_name='vPGA_kinase',
    function=v2,
    stoichiometry= {"ATP": -1, "PGA": -1, "BPGA": 1},
    modifiers=["ADP"],
    parameters=["k", "q2"],
    reversible=True,
)

model.add_reaction(
    rate_name='vBPGA_dehydrogenase',
    function=v3,
    stoichiometry= {"BPGA": -1, "NADPH": -1, "GAP": 1},
    dynamic_variables=["BPGA", "NADPH", "GAP", "NADP", "Pi"],
    parameters=["k", "H_stroma", "q3"],
    reversible=True,
)

model.add_reaction(
    rate_name='vTPI',
    function=v4,
    stoichiometry= {"GAP": -1, "DHAP": 1},
    parameters=["k", "q4"],
    reversible=True,
)

model.add_reaction(
    rate_name='vAldolase',
    function=v5,
    stoichiometry= {"GAP": -1, "DHAP": -1, "FBP": 1},
    parameters=["k", "q5"],
    reversible=True,
)

model.add_reaction(
    rate_name='vFBPase',
    function=v6,
    stoichiometry= {"FBP": -1, "F6P": 1},
    modifiers= ["Pi"],
    parameters=["V6", "Km6", "Ki61", "Ki62"],
    reversible=True,
)

model.add_reaction(
    rate_name='vF6P_Transketolase',
    function=v7,
    stoichiometry= {"GAP": -1, "F6P": -1, "X5P": 1, "E4P": 1},
    parameters=["k", "q7"],
    reversible=True,
)

model.add_reaction(
    rate_name='v8',
    function=v8,
    stoichiometry= {"DHAP": -1, "E4P": -1, "SBP": 1},
    parameters=["k", "q8"],
    reversible=True,
)

model.add_reaction(
    rate_name='v9',
    function=v9,
    stoichiometry= {"SBP": -1, "S7P": 1}, #CHECKME
    dynamic_variables=["SBP","Pi"],
    parameters=["V9", "Km9", "Ki9"],
    reversible=True,
)

model.add_reaction(
    rate_name='v10',
    function=v10,
    stoichiometry= {"GAP": -1, "S7P": -1, "X5P": 1, "R5P": 1}, 
    parameters=["k", "q10"],
    reversible=True,
)

model.add_reaction(
    rate_name='v11',
    function=v11,
    stoichiometry= {"R5P": -1, "RU5P": 1}, 
    parameters=["k", "q11"],
    reversible=True,
)

model.add_reaction(
    rate_name='v12',
    function=v12,
    stoichiometry= {"X5P": -1, "RU5P": 1}, 
    parameters=["k", "q12"],
    reversible=True,
)

model.add_reaction(
    rate_name='v13',
    function=v13,
    stoichiometry= {"RU5P": -1, "ATP": -1, "RUBP": 1}, 
    modifiers=["PGA", "Pi", "ADP"],
    parameters=["V13", "Km131", "Ki131", "Ki132", "Ki133", "Ki134", "Km132", "Ki135"],
    reversible=True,
)

model.add_reaction(
    rate_name='vG6P_isomerase',
    function=v14,
    stoichiometry= {"F6P": -1, "G6P": 1}, 
    parameters=["k", "q14"],
    reversible=True,
)

model.add_reaction(
    rate_name='vPhosphoglucomutase',
    function=v15,
    stoichiometry= {"G6P": -1, "G1P": 1}, 
    parameters=["k", "q15"],
    reversible=True,
)

model.add_reaction(
    rate_name='vpga',
    function=vpga,
    stoichiometry= {"PGA": -1}, 
    modifiers=["N"],
    parameters=["Vx", "Kpga"],
    reversible=True,
)

model.add_reaction(
    rate_name='vgap',
    function=vgap,
    stoichiometry= {"GAP": -1}, 
    modifiers=["N"],
    parameters=["Vx", "Kgap"],
    reversible=True,
)

model.add_reaction(
    rate_name='vdhap',
    function=vdhap,
    stoichiometry= {"DHAP": -1},
    modifiers=["N"],
    parameters=["Vx", "Kdhap"],
    reversible=True,
)

model.add_reaction(
    rate_name='vStarch',
    function=vStarch,
    stoichiometry= {"G1P": -1, "ATP": -1}, 
    modifiers= ["ADP", "Pi", "PGA", "F6P", "FBP"],
    parameters=["Vst", "Kmst1", "Kist", "Kmst2", "Kast1", "Kast2", "Kast3"],
    reversible=False,
)
