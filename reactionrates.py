import numpy as np


def calculate_pHinv(x):
    return (4e3*10**(-x))

def keq_PQred(E0_QA, F, E0_PQ, pHstroma, dG_pH, RT):
    DG1 = -E0_QA * F
    DG2 = -2 * E0_PQ * F
    DG = -2 * DG1 + DG2 + 2 * pHstroma * dG_pH
    K = np.exp(-DG/RT)
    return K

def Keq_cyc(E0_Fd, F, E0_PQ, pHstroma, dG_pH, RT):
    DG1 = -E0_Fd * F
    DG2 = -2 * E0_PQ * F
    DG = -2 * DG1 + DG2 + 2 * dG_pH * pHstroma
    K = np.exp(-DG/RT)
    return K

def Keq_FAFd(E0_FA, F, E0_Fd, RT):
    DG1 = -E0_FA * F
    DG2 = -E0_Fd * F
    DG = -DG1 + DG2
    K = np.exp(-DG/RT)
    return K
               
def Keq_PCP700(E0_PC, F, E0_P700, RT):
    DG1 = -E0_PC * F
    DG2 = -E0_P700 * F
    DG = -DG1 + DG2
    K = np.exp(-DG/RT)
    return K

def Keq_FNR(E0_Fd, F, E0_NADP, pHstroma, dG_pH, RT):
    DG1 = -E0_Fd * F
    DG2 = -2 * E0_NADP * F
    DG = -2 * DG1 + DG2 + dG_pH * pHstroma
    K = np.exp(-DG/RT)
    return K

def Keq_ATP(pH, DeltaG0_ATP, dG_pH, HPR, pHstroma, Pi_mol, RT):
    DG = DeltaG0_ATP - dG_pH * HPR * (pHstroma - pH)
    Keq = Pi_mol * np.exp(-DG/RT)
    return Keq

def Keq_cytb6f(pH, F, E0_PQ, E0_PC, pHstroma, RT, dG_pH):
    DG1 = -2 * F * E0_PQ
    DG2 = -F * E0_PC
    DG = - (DG1 + 2*dG_pH * pH) + 2 * DG2 + 2*dG_pH * (pHstroma - pH)
    Keq = np.exp(-DG/RT)
    return Keq

# Rate of electron flow through the photosystems.
# Calling algebraic modules calculating excited states of each photosystem
def vPS2(B1, k2):
    """ reaction rate constant for photochemistry """
    v = 0.5 * k2 * B1
    return v

def vPS1(A, LI):
    """ reaction rate constant for open PSI """
    v = LI * A
    return v

###############################################################################
# Reaction rates
###############################################################################
def _oxygen(time, ox, O2ext, kNDH, Ton, Toff):
    """ return oxygen and NDH concentration as a function of time
    used to simulate anoxia conditions as in the paper"""
    if ox == True:
        ''' by default we assume constant oxygen supply'''
        return O2ext, kNDH
    else:
        if time<Ton or time>Toff:
            return O2ext, 0
        else:
            return 0, kNDH

##############################################################################
def oxygen(time, ox, O2ext, kNDH, Ton, Toff):
    """ return oxygen and NDH concentration as a function of time
    used to simulate anoxia conditions as in the paper"""
    if isinstance(time, (int, float)):
        return np.array(_oxygen(time, ox, O2ext, kNDH, Ton, Toff))
    else:
        return np.array(
            [
                _oxygen(t, ox, O2ext, kNDH, Ton, Toff)
                for t in time
            ]
        ).T     
        
def vPTOX(Pred, time, kPTOX, ox, O2ext, kNDH, Ton, Toff):
    """ calculates reaction rate of PTOX """
    v = Pred * kPTOX * oxygen(time, ox, O2ext, kNDH, Ton, Toff)[0] 
    return v

def vNDH(Pox, time, ox, O2ext, kNDH, Ton, Toff):
    """ 
    calculates reaction rate of PQ reduction under absence of oxygen
    can be mediated by NADH reductase NDH
    """
    v = oxygen(time, ox, O2ext, kNDH, Ton, Toff)[1] * Pox
    return v

def vB6f(PC, Pox, H, Pred, PCred, pH, kCytb6f, F, E0_PQ, E0_PC, pHstroma, RT, dG_pH):
    """ calculates reaction rate of cytb6f """
    Keq = Keq_cytb6f(pH, F, E0_PQ, E0_PC, pHstroma, RT, dG_pH)
    v = np.maximum(kCytb6f * (Pred * PC**2 - (Pox * PCred**2)/Keq), -kCytb6f)
    return v

def vCyc(Pox, Fdred, kcyc):
    """
    calculates reaction rate of cyclic electron flow
    considered as practically irreversible
    """
    v = kcyc * ((Fdred**2) * Pox)
    return v

def vFNR(Fd, Fdred, NADPH, NADP, KM_FNR_F, KM_FNR_N, EFNR, kcatFNR, Keq_FNR, convf):
    """
    Reaction rate mediated by the Ferredoxin—NADP(+) reductase (FNR)
    Kinetic: convenience kinetics Liebermeister and Klipp, 2006
    Compartment: lumenal side of the thylakoid membrane
    Units:
    Reaction rate: mmol/mol Chl/s
    [F], [Fdred] in mmol/mol Chl/s
    [NADPH] in mM
    """
    fdred = Fdred/KM_FNR_F
    fdox = Fd/KM_FNR_F
    nadph = (NADPH/convf)/KM_FNR_N  # NADPH requires conversion to mmol/mol of chlorophyll 
    nadp = (NADP/convf)/KM_FNR_N # NADP requires conversion to mmol/mol of chlorophyll 
    v = (EFNR * kcatFNR *
        ((fdred**2) * nadp - ((fdox**2) * nadph) / Keq_FNR) /
        ((1+fdred+fdred**2) * (1+nadp) + (1+fdox+fdox**2) * (1+nadph) - 1))
    return v 

def vLeak(H, kLeak, pHstroma):
    """ 
    rate of leak of protons through the membrane
    """
    v = kLeak * (H - calculate_pHinv(pHstroma))
    return v

def vSt12(Ant, Pox, kStt7, PQtot, KM_ST, n_ST):
    """ 
    reaction rate of state transitions from PSII to PSI
    Ant depending on module used corresponds to non-phosphorylated antennae
    or antennae associated with PSII
    """
    kKin = kStt7 * ( 1 / (1 + ((Pox /PQtot)/KM_ST)**n_ST))
    v = kKin * Ant
    return v

def vSt21(LHCp, kPph1):
    """
    reaction rate of state transitions from PSI to PSII
    """
    v = kPph1 * LHCp
    return v

def vATPsynthase(ATP, ADP, pH, kATPsynth, DeltaG0_ATP, dG_pH, HPR, pHstroma,Pi_mol, RT, convf):
    """
    Reaction rate of ATP production
    Kinetic: simple mass action with PH dependant equilibrium
    Compartment: lumenal side of the thylakoid membrane
    Units:
    Reaction rate: mmol/mol Chl/s
    [ATP], [ADP] in mM
    """    
    v = kATPsynth * (ADP/convf - ATP/convf / Keq_ATP(pH, DeltaG0_ATP, dG_pH, HPR, pHstroma, Pi_mol, RT)) #* E
    return v

def vDeepox(Vx, H, nH, kDeepoxV, kphSat):
    """
    activity of xantophyll cycle: de-epoxidation of violaxanthin, modelled by Hill kinetics
    """
    vf = kDeepoxV * ((H ** nH)/ (H ** nH + calculate_pHinv(kphSat) ** nH)) * Vx
    return vf

def vEpox(Zx, kEpoxZ):
    """
    activity of xantophyll cycle: epoxidation
    """
    vr = kEpoxZ * Zx
    return vr

def vLhcprotonation(Psbs, H, nH, kProtonationL, kphSatLHC):
    """
    activity of PsbS protein protonation: protonation modelled by Hill kinetics
    """
    vf = kProtonationL * ((H ** nH)/ (H ** nH + calculate_pHinv(kphSatLHC) ** nH)) * Psbs
    return vf

def vLhcdeprotonation(Psbsp, kDeprotonation):
    """
    activity of PsbS protein protonation: deprotonation
    """
    vr = kDeprotonation * Psbsp
    return vr

""" Calvin-Benson-Bassham Cycle Reaction Rates """
def v1(RUBP, PGA, FBP, SBP, P, NADPH, V1, CO2, Km1, Ki11, Ki12, Ki13, Ki14, Ki15, KmCO2):
    """ rate of RuBisCO
    3 Ribulose-1,5-bisphosphate + 3 CO2
    -- RuBisCO -->
    6 3-Phosphoglycerate
    RuBp + CO2 -> PGA
    """
    return (V1*RUBP*CO2)/((RUBP+Km1*(1+(PGA/Ki11)+(FBP/Ki12)+(SBP/Ki13)+(P/Ki14)+(NADPH/Ki15)))*(CO2+KmCO2))

def v2(ATP, PGA, BPGA, ADP, k, q2):
    """
    6 3-Phosphoglycerate + 6 ATP
    -- Phosphoglycerate kinase (PGK) -->
    6 1,3-Bisphosphoglycerate + 6 ADP
    PGA + ATP -> BPGA + ADP
    Assumed to be at equilibrium
    """
    return k*((ATP*PGA)-(1/q2)*(ADP*BPGA))

def v3(BPGA, NADPH, GAP, NADP, P, k, H_stroma, q3):
    """
    6 1,3-Bisphosphoglycerate + 6 NADPH + 6 H+
    -- Glyceraldehyde 3-phosphate dehydrogenase (GADPH)-->
    1 G3P + 5 Glyceraldehyde 3-phosphate
    BPGA + NADPH -> GAP + NADP
    Assumed to be at equilibrium
    Stroma pH is assumed to be constant
    """

    return k*((NADPH*BPGA*H_stroma)-(1/q3)*(GAP*NADP*P))

def v4(GAP, DHAP, k, q4):
    """ 
    (5) Glyceraldehyde 3-phosphate
    -- Triose phosphate isomerae (TPI)-->
    (?) Dihydroxyacetone phosphate
    GAP -> DHAP
    Assumed to be at equilibrium
    """
    return k*((GAP)-(1/q4)*(DHAP))

def v5(GAP, DHAP, FBP, k, q5):
    """
    (5) Glyceraldehyde 3-phosphate + (?) Dihydroxyacetone phosphate
    -- Aldolase (ALD)-->
    Frucose 1,6-bisphosphate
    GAP + DHAP -> FBP
    Assumed to be at equilibrium
    """
    return k*((GAP*DHAP)-(1/q5)*(FBP))

def v6(FBP, F6P, P, V6, Km6, Ki61, Ki62):
    """
    (?) Fructose 1,6-bisphosphate + (?) H20
    --Fructose 1,6-bisphosphatase (FBPase) -->
    (6) Fructose 6-phosphate + (Pi)
    FBP -> F6P
    """
    return (V6*FBP)/(FBP+Km6*(1+(F6P/Ki61)+(P/Ki62)))

def v7(GAP, F6P, X5P, E4P, k, q7):
    """
    (?) Fructose 6-phosphate + (?) Glyceraldehyde 3-phosphate
    -- Transketolase (TK) -->
    (?) Xylulose 5-phosphate + (?) Erythrose 4-phosphate
    GAP + F6P -> X5P + E4P
    Assumed to be at equilibrium
    """
    return k*((GAP*F6P)-(1/q7)*(X5P*E4P))

def v8(DHAP, E4P, SBP, k, q8):
    """
    (?) Dihydroxyacetone phosphate + (?) Erythrose 4-phosphate
    -- Aldolase (ALD)-->
    (?) Sedoheptulose 1,7-bisphosphate
    DHAP + E4P -> SBP
    Assumed to be at equilibrium
    """
    return k*((DHAP*E4P)-(1/q8)*(SBP))

def v9(SBP, Pi, V9, Km9, Ki9):
    """
    (?) Sedoheptulose 1,7-bisphosphate + H20
    --Sedoheptulose 1,7-bisphosphatase (SBPase)-->
    (?) Sedoheptulose 7-phosphate + (?) Pi
    SBP -> S7P
    """

    return (V9*SBP)/(SBP+Km9*(1+(Pi/Ki9)))

def v10(GAP, S7P, X5P, R5P, k, q10):
    return k*((GAP*S7P)-(1/q10)*(X5P*R5P))

def v11(R5P, RU5P, k, q11):
    return k*((R5P)-(1/q11)*(RU5P))

def v12(X5P, RU5P, k, q12):
    return k*((X5P)-(1/q12)*(RU5P))

def v13(RU5P, ATP, RUBP, PGA, P, ADP, V13, Km131, Ki131, Ki132, Ki133, Ki134, Km132, Ki135):
    return (V13*RU5P*ATP)/((RU5P+Km131*(1+(PGA/Ki131)+(RUBP/Ki132)+(P/Ki133)))*(ATP*(1+(ADP/Ki134))+Km132*(1+(ADP/Ki135))))

def v14(F6P, G6P, k, q14):
    return k*((F6P)-(1/q14)*(G6P))

def v15(G6P, G1P, k, q15):
    return k*((G6P)-(1/q15)*(G1P))

def vpga(PGA, N, Vx, Kpga):
    return (Vx*PGA)/(N*Kpga)

def vgap(GAP, N, Vx, Kgap):
    return (Vx*GAP)/(N*Kgap)

def vdhap(DHAP, N, Vx, Kdhap):
    return (Vx*DHAP)/(N*Kdhap)

def vStarch(G1P, ATP, ADP, P, PGA, F6P, FBP, Vst, Kmst1, Kist, Kmst2, Kast1, Kast2, Kast3):
    """G1P -> Gn-1 ; Starch production"""
    return (Vst*G1P*ATP)/((G1P+Kmst1)*((1+(ADP/Kist))*(ATP+Kmst2)+((Kmst2*P)/(Kast1*PGA+Kast2*F6P+Kast3*FBP))))
